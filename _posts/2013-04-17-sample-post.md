---
title: Sample Post
layout: post
---

## Hi!

This is just a sample post.

Bacon ipsum dolor sit amet ground round tri-tip dolore, ullamco deserunt beef nostrud kielbasa reprehenderit. Boudin commodo veniam biltong. Occaecat adipisicing nulla, venison magna pancetta meatloaf laborum anim cupidatat id turkey dolor eu jowl. Beef fugiat exercitation meatloaf dolore irure ut sed t-bone. Cupidatat eiusmod pork loin laboris proident doner reprehenderit. Excepteur pariatur shoulder frankfurter chicken fugiat. Andouille veniam t-bone jowl, ad meatloaf biltong salami kielbasa aliqua aliquip dolore excepteur.

Syntax highlighting test (from SillyBox source):

{% highlight javascript %}

// Check if player is on right wall
if (player.x >= width-player.width) {
  player.x = width-player.width;
  onRightWall = true;
} else if (!(player.x >= width-player.width)) {
  onRightWall = false;
}
// Check if player is on left wall
if (player.x <= 0) {
  player.x = 0;
  onLeftWall = true;
} else if (!(player.x <= 0)) {
  onLeftWall = false;
}

{% endhighlight %}

Link test: [My current site.][adnils.com]

Ut dolore shank tempor, exercitation consequat corned beef. Pork minim ex drumstick, tongue in short ribs qui dolore filet mignon non sed cupidatat meatball. Pig tenderloin short loin, pork loin anim cupidatat jowl beef ribs capicola. Ad jerky leberkas in. Veniam capicola magna in quis. Beef ribs enim minim elit biltong non. Ex meatball pariatur hamburger dolore swine esse.

Flank pancetta tongue enim. Et incididunt reprehenderit meatball, tail sirloin do exercitation eiusmod consectetur elit. Consectetur magna chuck, esse tail pariatur tri-tip doner boudin veniam sed ut capicola leberkas strip steak. Ex in boudin, irure culpa id doner quis ut ground round shankle ea.

[adnils.com]: http://adnils.com
